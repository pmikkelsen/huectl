package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"os/user"
	"strconv"
	"strings"
)

type discoveryResult struct {
	Id                string
	Internalipaddress string
}

type newUser struct {
	DeviceType string `json:"devicetype"`
}

type lightData struct {
	State lightState `json:"state"`
	Name  string     `json:"name"`
}

type lightState struct {
	On        bool `json:"on"`
	Bri       int  `json:"bri"`
	Hue       int  `json:"hue"`
	Sat       int  `json:"sat"`
	Reachable bool `json:"reachable"`
}

func main() {
	args := os.Args[1:]

	bridgeIp, err := discoverBridge()
	if err != nil {
		fmt.Printf("Brigde discovery error: %s\n", err.Error())
		return
	}

	apiAddr := "http://" + bridgeIp + "/api"

	user, err := loadUserFile()
	if err != nil {
		fmt.Println("No user file found at ~/.huectl, trying to create new user..")
		user, err = createNewUser(apiAddr, "huectl")
		if err != nil {
			fmt.Printf("Could not create new user: %s\n", err.Error())
			return
		}
	}
	apiAddr = "http://" + bridgeIp + "/api/" + user

	lights, err := getLights(apiAddr)
	if err != nil {
		fmt.Println(err)
	}

	if len(args) == 0 {
		for id, data := range lights {
			printLightData(id, data)
		}
	} else {
		for _, arg := range args {
			if err != nil {
				fmt.Println(err)
				return
			}

			changes, err := handleArg(apiAddr, lights, arg)
			if err != nil {
				fmt.Println(err)
				return
			}

			if changes != "" {
				newLights, err := getLights(apiAddr)
				if err != nil {
					fmt.Println(err)
					return
				}
				lightId := strings.Split(arg, ".")[0]
				printFieldChange(lightId, lights[lightId], newLights[lightId], changes)

				lights = newLights
			}
		}
	}
}

func discoverBridge() (string, error) {
	resp, err := http.Get("https://discovery.meethue.com")
	if err != nil {
		return "", err
	} else {
		defer resp.Body.Close()
		body, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			return "", err
		} else {
			var jsonReply []discoveryResult
			err = json.Unmarshal(body, &jsonReply)
			if err != nil {
				return "", err
			} else {
				return jsonReply[0].Internalipaddress, nil
			}
		}
	}
}

func createNewUser(apiAddr string, devicetype string) (string, error) {
	m := newUser{devicetype}
	b, err := json.Marshal(m)
	if err != nil {
		return "", err
	}

	resp, err := http.Post(apiAddr, "application/json", bytes.NewReader(b))
	if err != nil {
		return "", err
	}

	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return "", err
	}

	trimmedBody := body[1 : len(body)-1]

	var j interface{}
	err = json.Unmarshal(trimmedBody, &j)
	if err != nil {
		return "", err
	}

	jsonData := j.(map[string]interface{})
	errorData, ok := jsonData["error"]
	if ok {
		errorDataMap := errorData.(map[string]interface{})
		return "", fmt.Errorf("%v", errorDataMap["description"])
	}

	successData, ok := jsonData["success"]
	if ok {
		successDataMap := successData.(map[string]interface{})
		username := successDataMap["username"].(string)

		user, err := user.Current()
		if err != nil {
			return "", err
		}

		err = ioutil.WriteFile(user.HomeDir+"/.huectl", []byte(username), 0644)
		if err != nil {
			return "", err
		}

		return username, nil
	}

	return "", fmt.Errorf("Unknown response from bridge")
}

func loadUserFile() (string, error) {
	user, err := user.Current()
	if err != nil {
		return "", err
	}

	username, err := ioutil.ReadFile(user.HomeDir + "/.huectl")
	if err != nil {
		return "", err
	}

	return string(username), nil
}

func getLights(apiAddr string) (map[string]lightData, error) {
	resp, err := http.Get(apiAddr + "/lights")
	if err != nil {
		return nil, err
	} else {
		defer resp.Body.Close()
		body, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			return nil, err
		}

		var lights map[string]lightData
		err = json.Unmarshal(body, &lights)
		if err != nil {
			return nil, err
		}

		return lights, nil
	}
}

func printLightData(id string, data lightData) {
	name := data.Name

	fmt.Printf("%s.name=%s\n", id, name)

	fields := []string{"on", "reachable", "hue", "satuation", "brightness"}
	for _, field := range fields {
		printField(id, data, field)
	}
}

func handleArg(apiAddr string, lights map[string]lightData, arg string) (string, error) {
	argParts := strings.Split(arg, "=")
	field := strings.Split(argParts[0], ".")
	lightId := field[0]
	data, ok := lights[lightId]
	if !ok {
		return "", fmt.Errorf("No such light: %s", lightId)
	}

	switch len(argParts) {
	case 1:
		err := printField(lightId, data, strings.Join(field[1:], "."))
		if err != nil {
			return "", err
		}
	case 2:
		err := setField(apiAddr, lightId, data, strings.Join(field[1:], "."), argParts[1])
		if err != nil {
			return "", err
		} else {
			return strings.Join(field[1:], "."), nil
		}
	default:
		return "", fmt.Errorf("Could not parse: %s", arg)
	}
	return "", nil
}

func printField(lightId string, data lightData, field string) error {
	if field == "" {
		printLightData(lightId, data)
	} else if field == "name" {
		name := data.Name
		fmt.Printf("%s.%s=%s\n", lightId, field, name)
	} else {
		value := fieldString(data, field)
		if value == "" {
			return fmt.Errorf("Field not valid %s", field)
		}

		fmt.Printf("%s.%s=%s\n", lightId, field, fieldString(data, field))
	}
	return nil
}

func printFieldChange(lightId string, old lightData, new lightData, field string) {
	fmt.Printf("%s.%s: %s -> %s\n", lightId, field, fieldString(old, field), fieldString(new, field))
}

func fieldString(data lightData, field string) string {
	state := data.State

	switch field {
	case "brightness":
		return fmt.Sprintf("%d", state.Bri)
	case "hue":
		return fmt.Sprintf("%d", state.Hue)
	case "satuation":
		return fmt.Sprintf("%d", state.Sat)
	case "on":
		return fmt.Sprintf("%t", state.On)
	case "reachable":
		return fmt.Sprintf("%t", state.Reachable)
	default:
		return ""
	}
}

func setField(apiAddr string, lightId string, data lightData, field string, valueStr string) error {
	lightApiAddr := apiAddr + "/lights/" + lightId + "/state"

	switch field {
	case "":
		return fmt.Errorf("Field %s is not settable", lightId+"."+field)
	case "on":
		value, err := strconv.ParseBool(valueStr)
		if err != nil {
			return err
		}
		data.State.On = value
	case "hue":
		value, err := strconv.Atoi(valueStr)
		if err != nil {
			return err
		}
		data.State.Hue = inRange(value, 0, 65535)
	case "satuation":
		value, err := strconv.Atoi(valueStr)
		if err != nil {
			return err
		}
		data.State.Sat = inRange(value, 0, 254)
	case "brightness":
		value, err := strconv.Atoi(valueStr)
		if err != nil {
			return err
		}
		data.State.Bri = inRange(value, 0, 254)
	default:
		return fmt.Errorf("set not implemented for field '%s'", field)
	}

	b, err := json.Marshal(data.State)
	if err != nil {
		return err
	}

	client := http.Client{}
	req, err := http.NewRequest("PUT", lightApiAddr, bytes.NewReader(b))
	if err != nil {
		return err
	}

	_, err = client.Do(req)
	if err != nil {
		return err
	}

	return nil
}

func inRange(value int, min int, max int) int {
	if value >= max {
		return max
	} else if value <= min {
		return min
	} else {
		return value
	}
}
